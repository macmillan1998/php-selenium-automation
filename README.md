# Instruções para uso

Baixar o JDK (sudo apt install default-jre)

sudo apt install php7.4-curl

sudo apt install php7.4-zip

sudo apt install php7.4-xml

composer install

## Instruções para execução

- Faça a execução do servidor do selenium (O mesmo se encontra na pasta PHPSelenium)

java -jar selenium-server-4.1.1.jar standalone

- Executar o Teste (segue exemplo da execução)

php vendor/bin/phpunit TestesUniversa/Secretaria/Captacao/MatriculaAlunoTest.php
