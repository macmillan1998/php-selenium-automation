<?php

use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\WebDriver;
use TestesUniversa\Login\PageObject\Login;
use TestesUniversa\Sistema\GestaoConfiguracoes\PageObject\GestaoConfiguracoes;
use PHPUnit\Framework\TestCase;


include_once(__DIR__ . '/../../variaveisGlobais.php');

class GestaoConfiguracoesTest extends TestCase
{
    public static WebDriver $driver;

    public static function setUpBeforeClass(): void
    {
        $host = 'http://localhost:4444/wd/hub';
        self::$driver = RemoteWebDriver::create($host, DesiredCapabilities::chrome());
        self::$driver->manage()->window()->maximize();
    }

    public function testGestaoConfiguracoes()
    {
        
        $paginaLogin = new Login(self::$driver);
        $paginaLogin->realizaLogin(Login, Senha);

        $config = new GestaoConfiguracoes(self::$driver);
        $config->verificaValorConfiguracao('VALIDAR_TITULOS_VENCIDOS_REMATRICULA');

   }
}