<?php

namespace TestesUniversa\Sistema\GestaoConfiguracoes\PageObject;

use TestesUniversa\Funcoes;
use Facebook\WebDriver\WebDriver;
use Facebook\WebDriver\WebDriverBy;
use Faker\Provider\Base;

class GestaoConfiguracoes extends Funcoes
{
    public WebDriver $driver;

    public function __construct(WebDriver $driver)
    {
        $this->driver = $driver;
    }

    public function verificaSeExisteConfiguracao($nomeConfig)
    {
        if(!$this->verificaUrl(UrlBase."sistema/sis-config")){
            $this->acessaLink("sistema/sis-config");
        }

        $this->procuraEPreencheUmElemento('cssSelector', '#dataTableSisConfig_filter > label > input', $nomeConfig);

        sleep(1);

        if( $this->verificaSeExisteTextoListaDataTables('cssSelector', "#dataTableSisConfig > tbody > tr > td.sorting_1", $nomeConfig) ){
            return True;
        }
        return False;
    }

    public function verificaValorConfiguracao($nomeConfig): self
    {

        if($this->verificaSeExisteConfiguracao($nomeConfig)){
            echo "<script>alert('Email enviado com Sucesso!);</script>";
        }

        return $this;
    }
}
