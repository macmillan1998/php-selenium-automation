<?php

namespace TestesUniversa\Secretaria\MatrizCurricular;

use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\WebDriver;
use TestesUniversa\Login\PageObject\Login;
use TestesUniversa\Secretaria\MatrizCurricular\PageObject\MatrizCurricular;
use PHPUnit\Framework\TestCase;

include_once(__DIR__ . '/../../variaveisGlobais.php');

class CadastraMatrizCurricularTest extends TestCase
{
    public static WebDriver $driver;

    public static function setUpBeforeClass(): void
    {
        $host = 'http://localhost:4444/wd/hub';
        self::$driver = RemoteWebDriver::create($host, DesiredCapabilities::chrome());
        self::$driver->manage()->window()->maximize();
    }

    public function testCadastraMatriz()
    {
        $paginaLogin = new Login(self::$driver);
        $paginaLogin->realizaLogin(Login, Senha);

        $paginaMatriz = new MatrizCurricular(self::$driver);

        if(cursoPadraoMatriz){
            $paginaMatriz->cadastraMatrizCurricular(cursoPadraoMatriz);
        }
   }
}