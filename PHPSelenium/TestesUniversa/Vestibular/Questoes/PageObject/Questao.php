<?php

namespace TestesUniversa\Secretaria\MatrizCurricular\PageObject;

use TestesUniversa\Funcoes;
use Facebook\WebDriver\WebDriver;

class MatrizCurricular extends Funcoes{

    public WebDriver $driver;

    public function __construct(WebDriver $driver)
    {
        $this->driver = $driver;
    }

    public function cadastraMatrizCurricular($nomeCurso, $disciplinas = []): self   
    {
        $this->acessaLink('matricula/acadperiodo-matriz-curricular/add');


        //selecionar curso
        $this->clicaElemento("id", "s2id_cursoId");
        $this->aguardaElementoInvisivel("cssSelector", ".select2-searching");
        $this->procuraEPreencheUmElemento('cssSelector', '#select2-drop > div > input', $nomeCurso);
        $this->aguardaElementoInvisivel("cssSelector", ".select2-searching");
        $this->escolheOptionSelect("cssSelector", "#select2-drop > ul > li", $nomeCurso);

        $this->aguardaElementoVisivel("cssSelector", '#periodo_1');

        // Descrição matriz
        $this->procuraEPreencheUmElemento('cssSelector', '#matCurDescricao', $nomeCurso.' - Matriz');
        
        $this->clicaElemento("id", "periodo_1");
        foreach($disciplinas as $disciplina){
            $this->clicaElemento("id", "s2id_discId");
            $this->aguardaElementoInvisivel("cssSelector", ".select2-searching");
            $this->procuraEPreencheUmElemento('cssSelector', '#select2-drop > div > input', $disciplina);
            $this->aguardaElementoInvisivel("cssSelector", ".select2-searching");
            $this->escolheOptionSelect("cssSelector", "#select2-drop > ul > li", $disciplina, true);
            $this->clicaElemento("id", "add_disc");
        }

        $this->clicaElemento("cssSelector", ".form-group > .btn.btn-primary");
        $this->aguardaElementoVisivel("cssSelector", '#MsgBoxBack');
        $this->clicaElemento("cssSelector", ".MessageBoxButtonSection > button");
    return $this;
    
}


}