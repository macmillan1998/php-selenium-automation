<?php

namespace TestesUniversa;

use Facebook\WebDriver\WebDriver;
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\WebDriverExpectedCondition;

class Funcoes
{

    public WebDriver $driver;

    public function __construct(WebDriver $driver)
    {
        $this->driver = $driver;
    }

    public function acessaLink($link): self
    {
        $this->driver
            ->get(UrlBase . $link);
        return $this;
    }
    public function verificaUrl($urlExperada)
    {
        return $this->driver->getCurrentURL() == $urlExperada ? True : False;

    }
    public function aguardaElementoVisivel(string $modo, string $locator): self
    {
        $this->driver
            ->wait()
            ->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::$modo($locator)));
        return $this;
    }
    public function aguardaElementoInvisivel(string $modo, string $locator): self
    {

        $this->driver
            ->wait()
            ->until(WebDriverExpectedCondition::invisibilityOfElementLocated(WebDriverBy::$modo($locator)));

        return $this;
    }
    public function aguardaElementoConterValor(string $modo, string $locator, $valor = ' '): self
    {

        $this->driver
            ->wait()
            ->until(WebDriverExpectedCondition::elementValueContains(WebDriverBy::$modo($locator), $valor));

        return $this;
    }
    public function preencheCpf(string $modo, string $locator, string $cpf): self
    {
        $this->driver
            ->findElement(WebDriverBy::$modo($locator))
            ->sendKeys($cpf)
            ->submit();
        return $this;
    }
    public function verificaCheckBoxTitulos($titulo, $modo, $locator)
    {
        $tipoTitulo = $titulo->findElement(WebDriverBy::cssSelector('div div:nth-of-type(2) > p'))->getText();

        if (strpos($tipoTitulo, '(Obrigatório)') || strpos($tipoTitulo, '(Opcional)')) {
            $tipoTitulo = substr($tipoTitulo, 0, strpos($tipoTitulo, " ("));
        }
        if ($titulo->findElement(WebDriverBy::$modo($locator))->isEnabled()) {
            if(in_array($tipoTitulo, tipoTitulos) && !$titulo->findElement(WebDriverBy::$modo($locator))->isSelected() ||
                !in_array($tipoTitulo, tipoTitulos) && $titulo->findElement(WebDriverBy::$modo($locator))->isSelected())
            {
                $titulo->findElement(WebDriverBy::$modo($locator))->click();
            }
        }
    }
    public function pegaTitulosMatricula(string $modo, string $locator): self
    {

        $titulos = $this->driver->findElements(WebDriverBy::$modo($locator . ' > div'));

        foreach ($titulos as $titulo) {
            $this->verificaCheckBoxTitulos($titulo, 'cssSelector', ' div > .gerarTitulo');

            //#Forma De Pagamento
            $titulo->findElement(WebDriverBy::cssSelector(' div div:nth-of-type(3) > div'))->click(); // clico para abrir o select da formaPagamento
            //seleciona a forma de pagamento de acordo  com a configurada na variável global no arquivo variaveisGlobais.php
            if(formaPagamento != null || formaPagamento != ''){
                $formaPagamento = $this->driver->findElements(WebDriverBy::cssSelector('#select2-drop > ul > li'));
                foreach($formaPagamento as $forma){
                    if($forma->findElement(WebDriverBy::cssSelector('div'))->getText() == formaPagamento){
                        $forma->findElement(WebDriverBy::cssSelector('div'))->click();
                        break;
                    }
                }
            }

            //# Seleção de Valores
            $titulo->findElement(WebDriverBy::cssSelector('div div:nth-of-type(4) > div'))->click();
            $valores = $this->driver
                ->findElements(WebDriverBy::cssSelector('#select2-drop > ul > li'));
            foreach ($valores as $valor) {
                $qntdParcelas = $valor->findElement(WebDriverBy::cssSelector('div'))->getText();
                $qntdParcelas = substr($valor->findElement(WebDriverBy::cssSelector('div'))->getText(), 0, strpos($qntdParcelas, ' x'));
                if (in_array($qntdParcelas, parcelas) != false) {
                    $valor->findElement(WebDriverBy::cssSelector('div'))->click();
                    break;
                }
            }

            if(formaPagamento != 'Cartão'){
                //# Seleção de Data de Vencimento
                $this->procuraEPreencheUmElemento("cssSelector", "div .inputVencimento > input", date('d/m/Y', strtotime(date('Y-m-d') . ' + 10 days')), $titulo);
                $this->clicaElemento("cssSelector", "a[href='#cursoFormacao']");
            }
        }

        return $this;
    }
    public function selecioneOptionQualquer(string $modo, string $locator)
    {

        $this->driver
            ->findElement(WebDriverBy::$modo($locator))
            ->click();

        $this->driver
            ->wait()
            ->until(WebDriverExpectedCondition::invisibilityOfElementLocated(WebDriverBy::cssSelector('.select2-searching')));

        $this->driver
            ->findElements(WebDriverBy::cssSelector('#select2-drop > ul > li'))[1]->click();
    }
    public function procuraEPreenche(array $modoLocatorPreencher): self
    {
        foreach ($modoLocatorPreencher as $key => $valor) {
            $modo = $valor;
            $this->driver
                ->findElement(WebDriverBy::$modo($valor['locator']))
                ->sendKeys($valor['dado']);
        }
        return $this;
    }

    public function procuraCampoSelect(string $modo, string $locator, string $textobusca):self 
    {
        $this->driver->findElement(WebDriverBy::$modo($locator))->sendKeys($textobusca);
        return $this;
    }

    public function procuraEPreencheUmElemento(string $modo, string $locator, string $campo, $elemento = null): self
    {
        if ($elemento != null) {
            $elemento->findElement(WebDriverBy::$modo($locator))->click()->sendKeys($campo);
        } else {
            $this->driver
                ->findElement(WebDriverBy::$modo($locator))->click()->sendKeys($campo);
        }
        return $this;
    }
    public function clicaElemento(string $modo, string $locator, $posicao = false): self
    {
        if (!$posicao) {
            $this->driver
                ->findElement(WebDriverBy::$modo($locator))
                ->click();
        } else {
            $this->driver
                ->findElements(WebDriverBy::$modo($locator))[$posicao]
                ->click();
        }
        return $this;
    }
    public function escolheOptionSelect(string $modo, string $locator, string $campoBusca, bool $matriz = false): self
    {
        $options = $this->driver
            ->findElements(WebDriverBy::$modo($locator));

        foreach ($options as $lista) {
            if ($lista->getText() == $campoBusca && !$matriz) {
                $lista->click();
                break;
            }
            else if(strpos($lista->getText(), $campoBusca) && $matriz){
                $lista->click();
                break;
            }
        }
        return $this;
    }

    public function selecioneOptionSelectQualquer(string $modo, string $locator): self
    {
        $options = $this->driver
            ->findElements(WebDriverBy::$modo($locator));

        if ($options) {
            $options[1]->click();
        }

        return $this;
    }

    public function escolheOptionSelectContemValor(string $modo, string $locator, string $campoBusca): self
    {
        $options = $this->driver
            ->findElements(WebDriverBy::$modo($locator));
        foreach ($options as $lista) {
            if (strpos($lista->getText(), substr($campoBusca, 1)) != '') {
                $lista->click();
                break;
            }
        }
        return $this;
    }

    public function preencheVagasTurnos(string $modo, string $locator, string $campo): self
    {

        if(in_array('Matutino', turnos)){
            $this->driver
            ->findElement(WebDriverBy::$modo($locator))->click()->sendKeys($campo);
            return $this;
        }
    }

    public function escolheItemLista(string $modo, string $locator, string $campo, string $modoAcao, string $locatorAcao)
    {
        $elementos = $this->driver->findElements(WebDriverBy::$modo($locator));

        foreach($elementos as $elemento){
            if($elemento->getText() == $campo){
                $this->clicaElemento($modoAcao, $locatorAcao);
                return true;
            }
        }
        return false;
    }
    
    public function escolheOptionSelectAsterisco(string $modo, string $locator, $campo)
    {
        $this->clicaElemento($modo, $locator);
        $this->aguardaElementoInvisivel("cssSelector",".select2-searching");
        $this->procuraEPreencheUmElemento('cssSelector', '#select2-drop > div > input', $campo ? $campo : '%%%%%%%%%%%%%%%');
        $this->aguardaElementoInvisivel("cssSelector", ".select2-searching");
         //caso a variável esteja definida, busca dela, caso não, escolhe qualquer uma
        $campo ?
        $this->escolheOptionSelect("cssSelector", "#select2-drop > ul > li", $campo) : 
        $this->selecioneOptionSelectQualquer("cssSelector", "#select2-drop > ul > li");
    }

    public function verificaQuantidadeItens(string $modo, string $locator)
    {
        return count($this->driver->findElements(WebDriverBy::$modo($locator)));  
    }

    public function verificaSeExisteTextoListaDataTables(string $modo, string $locator, string $campoBusca)
    {
        $itens = $this->driver
            ->findElements(WebDriverBy::$modo($locator));

        foreach ($itens as $item) {
            if (strpos($item->getText(), $campoBusca)) {
                return True;
            }
        }
        return False;
    }

    public function aguardaElementoConterAtributo(string $modo, string $locator, $attribute, $valorAtt)
    {
        $this->driver
        ->wait()
        ->until($this->driver->findElements(WebDriverBy::$modo($locator)->getAttribute($attribute)) == $valorAtt);
        return $this;
    }

    public function verificaElementoContemAtributo(string $modo, string $locator, $attribute, $valorAtt)
    {

        if($this->driver->findElements(WebDriverBy::$modo($locator)->getAttribute($attribute)) == $valorAtt){
            return true;
        }
        return false;
    }

} 
