<?php

namespace TestesUniversa\Login\PageObject;

use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\WebDriver;
use Facebook\WebDriver\WebDriverExpectedCondition;


class Login
{

    private WebDriver $driver;

    public function __construct(WebDriver $driver)
    {
        $this->driver = $driver;
    }

    public function realizaLogin(string $usuario, string $senha): void
    {
        $this->driver
        ->get(UrlBase."acesso/autenticacao/login");
        if($this->verificaSeEstaLogado($this->driver)){
            return;
        }
        
        $this->driver
        ->wait()
        ->until(WebDriverExpectedCondition::visibilityOf($this->driver->findElement(WebDriverBy::name('username')))); // aguardar aparecer o input de username

        $this->driver
        ->findElement(WebDriverBy::name('username'))
        ->sendKeys($usuario);

        $this->driver
        ->findElement(WebDriverBy::id('senha'))
        ->sendKeys($senha)
        ->submit();
        // $this->driver->findElement(WebDriverBy::xpath('//*[@id="login-form"]/div/div[2]/button'))->click();
    }

    public function verificaSeEstaLogado(WebDriver $driver){
        if(UrlBase.'acesso/autenticacao/login' != $driver->getCurrentURL()){
            return 1;
        };

    }

    public static function retornaDadosPessoa(){
        //inicia
        $curl = curl_init();
        $post =[
            'acao' => 'gerar_pessoa',
            'sexo' => 'I',
            'pontuacao' => 'S',
            'idade' => '0',
            'txt_qtde' => '1',
        ];
        curl_setopt_array($curl, [
            CURLOPT_URL => 'https://www.4devs.com.br/ferramentas_online.php',
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POSTFIELDS => $post
        ]);
        //executa e fecha requisição
        $response = curl_exec($curl);
        curl_close($curl);
        $json = json_decode($response,true);
        return $json[0];
    }
  
}
