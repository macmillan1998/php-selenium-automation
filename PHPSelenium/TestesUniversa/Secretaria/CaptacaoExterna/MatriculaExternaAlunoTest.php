<?php

use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\WebDriver;
use TestesUniversa\Login\PageObject\Login;
use TestesUniversa\Secretaria\CaptacaoExterna\PageObject\MatriculaExternaAluno;
use PHPUnit\Framework\TestCase;

include_once(__DIR__ . '/../../variaveisGlobais.php');

class MatriculaExternaAlunoTest extends TestCase
{
    public static WebDriver $driver;

    public static function setUpBeforeClass(): void
    {
        $host = 'http://localhost:4444';
        self::$driver = RemoteWebDriver::create($host, DesiredCapabilities::chrome());
        self::$driver->manage()->window()->maximize();
    }

    public function testMatriculaExternaAluno()
    {
        do{
            $paginaMatricula = new MatriculaExternaAluno(self::$driver);
            $paginaMatricula->matriculaAlunoCaptacaoExternaSimplificada();
        }while (False);

   }
}