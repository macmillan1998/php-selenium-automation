<?php

namespace TestesUniversa\Secretaria\CaptacaoExterna\PageObject;

use TestesUniversa\Funcoes;
use Facebook\WebDriver\WebDriver;
use Faker\Factory;

class MatriculaExternaAluno extends Funcoes
{
    public WebDriver $driver;

    public function __construct(WebDriver $driver)
    {
        $this->driver = $driver;
    }

    public function matriculaAlunoCaptacaoExternaSimplificada(): self
    {
        $faker = Factory::create('pt_BR');
        $this->acessaLink('matricula/acadgeral-aluno/captacao');

        //aguarda aparecer o campo de cpf
        $this->aguardaElementoVisivel('name', 'pesCpf');
        
        //preenche cpf captação interna
        $this->preencheCpf('id', 'pesCpf', $faker->cpf);

        $this->aguardaElementoVisivel('name', 'pesNome');

        //preenche nome
        $this->procuraEPreencheUmElemento("id", "pesNome", $faker->name);
        
        //PREENCHESEXO
        $this->clicaElemento("id", "s2id_pesSexo");
        $this->aguardaElementoVisivel("cssSelector", "#select2-drop");
        $this->escolheOptionSelect("cssSelector", "#select2-drop > ul > li", $faker->randomElement(['Masculino', 'Feminino']));
        
        $this->procuraEPreencheUmElemento("id", "pesNaturalidade", $faker->city);
        
        $estado = $faker->state;
        $this->clicaElemento("id", "s2id_pesNascUf");
        $this->aguardaElementoVisivel("cssSelector", "#select2-drop");
        $this->procuraEPreencheUmElemento('cssSelector', '#select2-drop > div > input', $estado);
        $this->aguardaElementoVisivel("cssSelector", ".select2-highlighted");
        $this->escolheOptionSelectContemValor("cssSelector", "#select2-drop > ul > li", $estado);

        $this->procuraEPreencheUmElemento("id", "pesRg", $faker->stateAbbr . $faker->numerify('-##.###.###'));
        
        $this->procuraEPreencheUmElemento("id", "alunoMae", $faker->name('female'));
        
        $this->procuraEPreencheUmElemento("id", "alunoPai", $faker->name('male'));

        //preenche nascimento
        $this->procuraEPreencheUmElemento("id", "pesDataNascimento", $faker->date($format = 'd-m-Y', $max = '-18 years'));
        // Primeiro Passo Dados Pessoais 
                
        ##Contato e endereço
        $this->clicaElemento("cssSelector", "a[href='#contatoEndereco']");

        //preenche cep
        $this->procuraEPreencheUmElemento("id", "endCep", '3530011'.$faker->numerify('#'));
        //preenche celular
        $this->procuraEPreencheUmElemento("name", "conContatoCelular", '33999999999');

        $this->procuraEPreencheUmElemento("name", "endNumero", $faker->numberBetween(1, 1000));
        
        //preenche email
        $this->procuraEPreencheUmElemento("name", "conContatoEmail", "teste@phpselenium.com");

        //aguarda preencher endereço pelo cep
        $this->aguardaElementoConterValor("name", "endLogradouro");

        ##Curso e Pagamento
        $this->clicaElemento("cssSelector", "a[href='#cursoFormacao']");

        //formacao
        $this->procuraEPreencheUmElemento("id", "formacaoInstituicao", '-');

        //nivel
        $this->clicaElemento("id","s2id_nivelEnsino");
        $this->aguardaElementoInvisivel("cssSelector",".select2-searching");
        $this->escolheOptionSelect("cssSelector","#select2-drop > ul > li", 'Pós-Graduação Lato Sensu');

        //curso
        if(!curso){
            $this->selecioneOptionQualquer("id", "s2id_campusCurso");
        }
        else{
            $this->clicaElemento("id","s2id_campusCurso");
            $this->aguardaElementoInvisivel("cssSelector",".select2-searching");
            $this->procuraEPreencheUmElemento('cssSelector', '#select2-drop > div > input', curso);
            $this->aguardaElementoVisivel("cssSelector", ".select2-highlighted");
            $this->escolheOptionSelectContemValor("cssSelector","#select2-drop > ul > li", curso);
            
        }

        //aguarda o overlay depois de sair 
        $this->aguardaElementoInvisivel("cssSelector","app-overlay");
        $this->aguardaElementoVisivel("cssSelector","#tipoTitulo > div.list.componente-list.ui-sortable");
        $this->pegaTitulosMatricula("cssSelector", "#tipoTitulo > div.list.componente-list.ui-sortable");

        // finalizar a matrícula
        $this->clicaElemento("cssSelector", ".btn.btn-primary.submit");
        $this->aguardaElementoVisivel("cssSelector","#form-captacao-inicio");
        return $this;
    }
}
