<?php

namespace TestesUniversa\Secretaria\Curso\PageObject;

use TestesUniversa\Funcoes;
use Facebook\WebDriver\WebDriver;
use TestesUniversa\Secretaria\Disciplina\PageObject\CadastroDisciplina;
use TestesUniversa\Secretaria\MatrizCurricular\PageObject\MatrizCurricular;


class CadastroCurso extends Funcoes{

    public WebDriver $driver;

    public function __construct(WebDriver $driver)
    {
        $this->driver = $driver;
    }

    public static function escolheNomeCurso(){
        if (file_exists('TestesUniversa/Secretaria/curso/CursosNovos.txt')) {
            $todosCursos = explode("\n", file_get_contents('TestesUniversa/Secretaria/curso/CursosNovos.txt'));
            $cursoNome = $todosCursos[0];
            array_shift($todosCursos);
            $todosCursosString = implode("\n", $todosCursos);
            $file = fopen('TestesUniversa/Secretaria/curso/CursosNovos.txt', 'w');
            fwrite($file,  print_r($todosCursosString, TRUE));
            fclose($file);
            $file = fopen('TestesUniversa/Secretaria/curso/CursosCadastrados.txt', 'a+');
            fwrite($file,  print_r($cursoNome."\n", TRUE));
            fclose($file);
            return $cursoNome;
        }
        else{
            return false;
        }
    }

    public function cadastraCurso($nomeCurso): self
    {
        $this->acessaLink('matricula/acad-curso/add');
        $this->procuraEPreencheUmElemento('cssSelector', '#cursoNome', $nomeCurso . ' - PHP-SELENIUM'); //nome do curso
        $this->procuraEPreencheUmElemento('cssSelector', '#cursoNomeOficial', $nomeCurso . ' - PHP-SELENIUM - Oficial'); //nome oficial do curso
        $this->procuraEPreencheUmElemento('cssSelector', '#cursoSigla', mb_substr($nomeCurso, 0, 5)); //sigla do curso
        $this->procuraEPreencheUmElemento('cssSelector', '#cursoCargaHorariaPratica', cargaHorariaPraticaCurso); //Carga horária Prática
        $this->procuraEPreencheUmElemento('cssSelector', '#cursoCargaHorariaTeorica', cargaHorariaTeoricaCurso); //Carga horária Teórica
        //seleciona a modalidade
        $this->clicaElemento("id", "s2id_mod");
        $this->aguardaElementoInvisivel("cssSelector", ".select2-searching");
        $this->escolheOptionSelect("cssSelector", "#select2-drop > ul > li", modalidade);

        //seleciona a integralização
        $this->clicaElemento("id", "s2id_cursoUnidadeMedida");
        $this->aguardaElementoInvisivel("cssSelector", ".select2-searching");
        $this->escolheOptionSelect("cssSelector", "#select2-drop > ul > li", integralizacao);        

        $this->procuraEPreencheUmElemento('cssSelector', '#cursoPrazoIntegralizacao', cursoPrazoIntegralizacao); //Tempo de integralização Min
        $this->procuraEPreencheUmElemento('cssSelector', '#cursoPrazoIntegralizacaoMaxima', cursoPrazoIntegralizacaoMaxima); //Tempo de integralização Max

        //seleciona o nível de ensino
        $this->clicaElemento("id", "s2id_nivel");
        $this->aguardaElementoInvisivel("cssSelector", ".select2-searching");
        $this->escolheOptionSelect("cssSelector", "#select2-drop > ul > li", nivelCurso);   
        
        //seleciona se haverá período letivo
        $this->clicaElemento("id", "s2id_cursoPossuiPeriodoLetivo");
        $this->aguardaElementoInvisivel("cssSelector", ".select2-searching");
        $this->escolheOptionSelect("cssSelector", "#select2-drop > ul > li", temPeriodo);  

        //seleciona área de conhecimento
        $this->clicaElemento("id", "s2id_area");
        $this->aguardaElementoInvisivel("cssSelector", ".select2-searching");
        $this->escolheOptionSelect("cssSelector", "#select2-drop > ul > li", areaConhecimento);  

        //seleciona turnos
        foreach(turnos as $turno){
            $this->clicaElemento("id", "s2id_cursoTurno");
            $this->aguardaElementoInvisivel("cssSelector", ".select2-searching");
            $this->escolheOptionSelect("cssSelector", "#select2-drop > ul > li", $turno);
        }

        //vagas dos turnos
        foreach(vagasTurnos as $turno => $locator){
            if(in_array($turno,turnos)){
                $this->procuraEPreencheUmElemento('id', $locator, 100);
            }
        }

        //seleciona o coordenador do curso
        $this->clicaElemento("id", "s2id_pes");
        $this->aguardaElementoInvisivel("cssSelector", ".select2-searching");
        $this->procuraEPreencheUmElemento("cssSelector", "#select2-drop > div > input", '%');
        $this->aguardaElementoInvisivel("cssSelector", ".select2-searching");
        $this->clicaElemento("cssSelector", "#select2-drop > ul > li", 1);

        //data início curso
        $this->procuraEPreencheUmElemento("id", "cursoDataFuncionamento", date('d/m/Y'));

        //seleciona curso ativo ou não
        $this->clicaElemento("id", "s2id_cursoSituacao");
        $this->aguardaElementoInvisivel("cssSelector", ".select2-searching");
        $this->escolheOptionSelect("cssSelector", "#select2-drop > ul > li", 'Ativo');   

        //passo 2
        $this->clicaElemento("cssSelector", "a[href='#abaOutrasConfiguracoes']");

        // campus
        $this->clicaElemento("id", "s2id_campus");
        $this->aguardaElementoInvisivel("cssSelector", ".select2-searching");
        $this->clicaElemento("cssSelector", "#select2-drop > ul > li", 0); 
        
        //titulos obrigatórios
        if(titulosObrigatorios != []){
            foreach(titulosObrigatorios as $titulo){
                $this->clicaElemento("id", "s2id_tipotituloObrigatorio");
                $this->aguardaElementoInvisivel("cssSelector", ".select2-searching");
                $this->escolheOptionSelect("cssSelector", "#select2-drop > ul > li", $titulo);
            }
        }

        // obrigatorio externamente
        if(titulosObrigatoriosExterno != []){
            foreach(titulosObrigatoriosExterno as $titulo){
                $this->clicaElemento("id", "s2id_tipotituloOpcionalInternamente");
                $this->aguardaElementoInvisivel("cssSelector", ".select2-searching");
                $this->escolheOptionSelect("cssSelector", "#select2-drop > ul > li", $titulo);
            }
        }

        // opcionais
        if(titulosOpcionais != []){
            foreach(titulosOpcionais as $titulo){
                $this->clicaElemento("id", "s2id_tipotituloOpcional");
                $this->aguardaElementoInvisivel("cssSelector", ".select2-searching");
                $this->escolheOptionSelect("cssSelector", "#select2-drop > ul > li", $titulo);
            }
        }
        //abre modal configurações curso  
        $this->clicaElemento("id", "btn-add-configuracao");
        
        $this->aguardaElementoVisivel("cssSelector", ".modal-backdrop.fade.in");
        
        // Deferimento automático
        $this->clicaElemento("id", "s2id_cursoconfigDeferimentoAutomatico");
        $this->aguardaElementoInvisivel("cssSelector", ".select2-searching");
        $this->escolheOptionSelect("cssSelector", "#select2-drop > ul > li", deferimentoAuto);
        
        // bloqueio por inadimplência
        $this->clicaElemento("id", "s2id_cursoconfigBloqueioInadimplencia");
        $this->aguardaElementoInvisivel("cssSelector", ".select2-searching");
        $this->escolheOptionSelect("cssSelector", "#select2-drop > ul > li", bloqueioInadimplencia);

        //bloqueio por documentos
        $this->clicaElemento("id", "s2id_documentosBloqueio");
        $this->aguardaElementoInvisivel("cssSelector", ".select2-searching");
        $this->escolheOptionSelect("cssSelector", "#select2-drop > ul > li", bloqueioDocumentos);

        //tempo de reativação de curso 
        $this->procuraEPreencheUmElemento("id", "cursoconfigTempoReativacao", tempoReativacao);
        
        // situação pós deferimento
        $this->clicaElemento("id", "s2id_cursoconfigSituacaoDeferimento");
        $this->aguardaElementoInvisivel("cssSelector", ".select2-searching");
        $this->escolheOptionSelect("cssSelector", "#select2-drop > ul > li", posDeferimento);
        
        // situação pós matricula
        $this->clicaElemento("id", "s2id_cursoconfigSituacaoPosMatricula");
        $this->aguardaElementoInvisivel("cssSelector", ".select2-searching");
        $this->escolheOptionSelect("cssSelector", "#select2-drop > ul > li", posMatricula);

        // regime de disciplinas
        $this->clicaElemento("id", "s2id_cursoconfigRegimeDisciplinas");
        $this->aguardaElementoInvisivel("cssSelector", ".select2-searching");
        $this->escolheOptionSelect("cssSelector", "#select2-drop > ul > li", regimeDisciplinas);
        
        // método de avaliação (nota, conceito)
        $this->clicaElemento("id", "s2id_cursoconfigMetodoAvaliacao");
        $this->aguardaElementoInvisivel("cssSelector", ".select2-searching");
        $this->escolheOptionSelect("cssSelector", "#select2-drop > ul > li", metodoAvaliacao);
        
        if(metodoAvaliacao != 'Conceito'){ //opção conceito desabilita todos os campos abaixo
            if(regimeDisciplinas == 'Crédito'){
                //mínimo de crédito
                $this->procuraEPreencheUmElemento("id", "cursoconfigMinimoCredito", minCredito);
                // máximo de crédito
                $this->procuraEPreencheUmElemento("id", "cursoconfigMaximoCredito", maxCredito);     
            }
            
            // Máx de pendências
            $this->procuraEPreencheUmElemento("id", "cursoconfigNumeroMaximoPendencias", maxPendencia); 
            
            // Frequência mínima
            $this->procuraEPreencheUmElemento("id", "cursoconfigFreqMin", minFrequencia);   
            
            //Nota máxima
            $this->procuraEPreencheUmElemento("id", "cursoconfigNotaMax", notaMax);     
            
            //Nota máxima
            $this->procuraEPreencheUmElemento("id", "cursoconfigNotaMin", notaAprovacao);   
            
            // método para cálculo de notas
            $this->clicaElemento("id", "s2id_cursoconfigMetodo");
            $this->aguardaElementoInvisivel("cssSelector", ".select2-searching");
            $this->escolheOptionSelect("cssSelector", "#select2-drop > ul > li", calculoNotas);
            
            //Casas decimais para nota 
            $this->procuraEPreencheUmElemento("id", "cursoconfigNotaFracionada", qntdCasasDecimais);  
            
            //min para realizar o exame final 
            $this->procuraEPreencheUmElemento("id", "cursoconfigNotaFinal", minNotaFinal); 
            
            //min para aprovação no exame final 
            $this->procuraEPreencheUmElemento("id", "cursoconfigMediaFinalMin", minAprovacaoFinal); 
            
            //método de cálculo do exame final 
            $this->clicaElemento("id", "s2id_cursoconfigMetodoFinal");
            $this->aguardaElementoInvisivel("cssSelector", ".select2-searching");
            $this->escolheOptionSelect("cssSelector", "#select2-drop > ul > li", metodoCalculoFinal);
        }  

        // Encerramento de etapa
        $this->clicaElemento("id", "s2id_cursoconfigEncerramentoEtapas");
        $this->aguardaElementoInvisivel("cssSelector", ".select2-searching");
        $this->escolheOptionSelect("cssSelector", "#select2-drop > ul > li", encerramentoEtapa);
            
        //data de início da configuração de curso
        $this->procuraEPreencheUmElemento("id", "cursoconfigDataInicio", date('d/m/Y'));

        $this->clicaElemento("id", "btn-salvar-configuracao");
        
        $this->clicaElemento("cssSelector", ".btn.btn-primary.salvar");

        if(criaDiscEMatriz){
            $this->cadastraMatriz($nomeCurso);
        }
        if(vinculaMatriz){
            $this->vinculaMatriz($nomeCurso);
        }
        return $this;
    }

    public function cadastraMatriz($nomeCurso): self
    {
        $discCurso = [];

        $criaDisciplinas = new CadastroDisciplina($this->driver);
        for($i = 0 ; $i < qntdDisc ; $i++){
            $discNome = $criaDisciplinas::escolheNomeDisciplina();
            $criaDisciplinas->cadastraDisciplina($discNome ,$nomeCurso);
            array_push($discCurso, $discNome . ' - PHP-Selenium');
        }

        $criaMatriz = new MatrizCurricular($this->driver);
        $criaMatriz->cadastraMatrizCurricular($nomeCurso . ' - PHP-SELENIUM', $discCurso);

        return $this;

    }

    public function vinculaMatriz($nomeCurso)
    {
        $this->acessaLink('matricula/acad-curso');
        $this->procuraEPreencheUmElemento("cssSelector", "#dataTableAcadCurso_filter > label > input", $nomeCurso);

        //caso tenha mais de um curso, ele procura o curso com nome idêntico
        if($this->escolheItemLista('cssSelector', '#dataTableAcadCurso > tbody > tr > td:nth-child(2)', $nomeCurso . ' - PHP-SELENIUM', 'cssSelector', '#dataTableAcadCurso > tbody > tr > td:last-child > div > div > .acadCurso-edit')){
            //abre o passo 2
            $this->clicaElemento("cssSelector", "a[href='#abaOutrasConfiguracoes']");

            //abre modal configurações curso  
            $this->clicaElemento("cssSelector", ".btn-item-edit");
            $this->aguardaElementoVisivel("cssSelector", ".modal-backdrop.fade.in");
           
            //seleciona matriz padrão
            $this->clicaElemento("id", "s2id_matrizCurricularId");
            $this->aguardaElementoInvisivel("cssSelector", ".select2-searching");
            $this->escolheOptionSelect("cssSelector", "#select2-drop > ul > li", $nomeCurso . ' - PHP-SELENIUM - Matriz');

            $this->clicaElemento("cssSelector", "#btn-salvar-configuracao"); //Modal
            $this->clicaElemento("cssSelector", "#acad-curso-form > div.col-sm-12.text-center > div > button"); //curso

        }

        //;
    }

}