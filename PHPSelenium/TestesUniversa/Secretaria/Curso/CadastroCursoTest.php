<?php

use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\WebDriver;
use TestesUniversa\Login\PageObject\Login;
use TestesUniversa\Secretaria\Curso\PageObject\CadastroCurso;
use PHPUnit\Framework\TestCase;
use Faker\Factory;


include_once(__DIR__ . '/../../variaveisGlobais.php');

class CadastroCursoTest extends TestCase
{
    public static WebDriver $driver;

    public static function setUpBeforeClass(): void
    {
        $host = 'http://localhost:4444/wd/hub';
        self::$driver = RemoteWebDriver::create($host, DesiredCapabilities::chrome());
        self::$driver->manage()->window()->maximize();
    }

    public function testCriaCurso()
    {
        
        $paginaLogin = new Login(self::$driver);
        $paginaLogin->realizaLogin(Login, Senha);
        
        $paginaCadastraCurso = new CadastroCurso(self::$driver);
        $faker = Factory::create('pt_BR');
        
        if($faker->course()){
            $paginaCadastraCurso->cadastraCurso($faker->course());
        }
        else{
            echo 'Não existe o arquivo com os nomes dos cursos!';
        }
   }
}