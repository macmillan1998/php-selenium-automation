<?php

namespace TestesUniversa\Secretaria\Disciplina;

use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\WebDriver;
use TestesUniversa\Login\PageObject\Login;
use TestesUniversa\Secretaria\Disciplina\PageObject\CadastroDisciplina;
use PHPUnit\Framework\TestCase;
use Faker\Factory;


include_once(__DIR__ . '/../../variaveisGlobais.php');

class CadastroDisciplinasTest extends TestCase
{
    public static WebDriver $driver;

    public static function setUpBeforeClass(): void
    {
        $host = 'http://localhost:4444/wd/hub';
        self::$driver = RemoteWebDriver::create($host, DesiredCapabilities::chrome());
        self::$driver->manage()->window()->maximize();
    }

    public function testCriaDisciplina()
    {
        
        $paginaLogin = new Login(self::$driver);
        $paginaLogin->realizaLogin(Login, Senha);

        $faker = Factory::create('pt_BR');

        $paginaCadastraDisc = new CadastroDisciplina(self::$driver);
        //$nomeDisc = $paginaCadastraDisc::escolheNomeDisciplina();
        
        if($faker->course()){
            $paginaCadastraDisc->cadastraDisciplina($faker->course());
        }
        else{
            echo 'Não existe o arquivo com os nomes dos cursos!';
        }
   }
}