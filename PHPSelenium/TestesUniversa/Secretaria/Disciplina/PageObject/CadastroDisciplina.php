<?php

namespace TestesUniversa\Secretaria\Disciplina\PageObject;

use TestesUniversa\Funcoes;
use Facebook\WebDriver\WebDriver;

class CadastroDisciplina extends Funcoes{

    public WebDriver $driver;

    public function __construct(WebDriver $driver)
    {
        $this->driver = $driver;
    }

    public static function escolheNomeDisciplina(){
        if (file_exists('TestesUniversa/Secretaria/disciplina/disciplinas.txt')) {
            $todasDisc = explode("\n", file_get_contents('TestesUniversa/Secretaria/disciplina/disciplinas.txt'));
            $discNome = $todasDisc[0];
            array_shift($todasDisc);
            $todasDiscString = implode("\n", $todasDisc);
            $file = fopen('TestesUniversa/Secretaria/disciplina/disciplinas.txt', 'w');
            fwrite($file,  print_r($todasDiscString, TRUE));
            fclose($file);
            $file = fopen('TestesUniversa/Secretaria/disciplina/disciplinasUsadas.txt', 'a+');
            fwrite($file,  print_r($discNome."\n", TRUE));
            fclose($file);
            return $discNome;
        }
        else{
            return false;
        }
    }

    public function cadastraDisciplina($discNome, $nomeCurso = false): self   
    {
        $this->acessaLink('matricula/acadgeral-disciplina/add');

        //preenche nome da disciplina
        $this->procuraEPreencheUmElemento('id', 'discNome', $discNome.' - PHP-Selenium');

        //preenche nome oficial da disciplina
        $this->procuraEPreencheUmElemento('id', 'discNomeOficial', $discNome.' - PHP-Selenium - Oficial');

        //Sigla da disciplina
        $this->procuraEPreencheUmElemento('cssSelector', '#discSigla', mb_substr($discNome, 0, 5));

        //Área de conhecimento disciplina
        $this->clicaElemento("id", "s2id_areaConhecimento");
        $this->aguardaElementoInvisivel("cssSelector", ".select2-searching");
        $this->escolheOptionSelect("cssSelector", "#select2-drop > ul > li", areaConhecimentoDisc);

        if($nomeCurso || nomeCurso){
            //abre modal e aguarda carregar para preencher
            $this->clicaElemento("id", "btn-add-curso");
            $this->aguardaElementoVisivel("cssSelector", ".modal-backdrop.fade.in");

            //selecionar curso
            $this->clicaElemento("id", "s2id_cursos");
            $this->aguardaElementoInvisivel("cssSelector", ".select2-searching");
            $this->procuraEPreencheUmElemento('cssSelector', '#select2-drop > div > input', $nomeCurso ? $nomeCurso . ' - PHP-SELENIUM' : nomeCurso);
            $this->aguardaElementoInvisivel("cssSelector", ".select2-searching");
            $this->escolheOptionSelect("cssSelector", "#select2-drop > ul > li", $nomeCurso ? $nomeCurso . ' - PHP-SELENIUM' : nomeCurso);

            //selecionar tipo da disciplina
            $this->clicaElemento("id", "s2id_tdiscId");
            $this->aguardaElementoInvisivel("cssSelector", ".select2-searching");
            $this->escolheOptionSelect("cssSelector", "#select2-drop > ul > li", tipoDisc);
            if(regimeDisciplinas == 'Crédito'){
                if(CreditosPadrao){
                    $this->procuraEPreencheUmElemento('cssSelector', '#credito', CreditosPadrao);
                }
                else{
                    $this->procuraEPreencheUmElemento('cssSelector', '#credito', rand(2, 5));
                }
            }
                $this->clicaElemento("id", "btn-enviar-curso");
        }
        $this->clicaElemento("id", "enviar-dados");
        $this->aguardaElementoVisivel("cssSelector", '#MsgBoxBack');
        $this->clicaElemento("cssSelector", ".MessageBoxButtonSection > button");

        
    return $this;
    
}


}