<?php

namespace TestesUniversa\Secretaria\MatriculaRematricula\PageObject;

use TestesUniversa\Funcoes;
use Facebook\WebDriver\WebDriver;
use Faker\Factory;

const TURMA = false;
const PERIODO = false;
const CURSO = 'Curso Presencial com Periodo Letivo';
const UNIDADE_ESTUDO = false;

class MatriculaRematricula extends Funcoes
{
    public WebDriver $driver;

    public function __construct(WebDriver $driver)
    {
        $this->driver = $driver;
    }

    public function matriculaRematricula(): self
    {
        $faker = Factory::create('pt_BR');
        $this->acessaLink('matricula/acadperiodo-aluno/add');

        //aguarda aparecer o campo de cpf
        $this->aguardaElementoVisivel('name', 'pesCpf');
        
        //preenche cpf captação interna
        $this->procuraEPreencheUmElemento('id', 'pesDocumento', $faker->cpf);
        $this->clicaElemento("cssSelector","[class='btn btn-default buscarDocumentoPessoa']");
        $this->aguardaElementoVisivel('cssSelector', '.app-overlay');
        $this->aguardaElementoInvisivel('cssSelector', '.app-overlay');

        //preenche nome
        $this->procuraEPreencheUmElemento("name", "pesNome", $faker->name);

        //campus
        $this->clicaElemento("id","s2id_autogen19");
        $this->aguardaElementoInvisivel("cssSelector",".select2-searching");
        $this->escolheOptionSelect("cssSelector","#select2-drop > ul > li", 'PRIMEIRO CÂMPUS INTEGRAÇÃO COM O AVA');

        //curso
        if(!CURSO){
            $this->selecioneOptionQualquer("id", "s2id_autogen21");
        }
        else{
            $this->clicaElemento("id","s2id_autogen21");
            $this->aguardaElementoInvisivel("cssSelector",".select2-searching");
            $this->procuraEPreencheUmElemento('cssSelector', '#select2-drop > div > input', CURSO);
            $this->aguardaElementoVisivel("cssSelector", ".select2-highlighted");
            $this->escolheOptionSelectContemValor("cssSelector","#select2-drop > ul > li > div", CURSO);
            
        }

        ##Dados Pessoais
        $this->clicaElemento("cssSelector", "a[href='#tab2']");

        //PREENCHESEXO
        $this->clicaElemento("id", "s2id_autogen34");
        $this->aguardaElementoVisivel("cssSelector", "#select2-drop");
        $this->escolheOptionSelect("cssSelector", "#select2-drop > ul > li", $faker->randomElement(['Masculino', 'Feminino']));
        
        $this->procuraEPreencheUmElemento("id", "alunoMae", $faker->name('female'));
        $this->procuraEPreencheUmElemento("id", "alunoPai", $faker->name('male'));

        //preenche nascimento
        $this->procuraEPreencheUmElemento("id", "pesDataNascimento", $faker->date($format = 'd-m-Y', $max = '-18 years'));

        $this->procuraEPreencheUmElemento("id", "pesNaturalidade", $faker->city);

        $estado = $faker->state;
        $this->clicaElemento("id", "s2id_pesNascUf");
        $this->aguardaElementoVisivel("cssSelector", "#select2-drop");
        $this->procuraEPreencheUmElemento('cssSelector', '#select2-drop > div > input', $estado);
        $this->aguardaElementoVisivel("cssSelector", ".select2-highlighted");
        $this->escolheOptionSelectContemValor("cssSelector", "#select2-drop > ul > li", $estado);

        $this->procuraEPreencheUmElemento("id", "pesRg", $faker->stateAbbr . $faker->numerify('-##.###.###'));
          
        ##Contato e endereço
        $this->clicaElemento("cssSelector", "a[href='#tab3']");

        //preenche cep
        $this->procuraEPreencheUmElemento("id", "endCep", '3530011'.$faker->numerify('#'));
        $this->clicaElemento("cssSelector", "[class='btn btn-default buscarCep']");
        $this->aguardaElementoVisivel('cssSelector', '.app-overlay');
        $this->aguardaElementoInvisivel('cssSelector', '.app-overlay');

        //preenche celular
        $this->procuraEPreencheUmElemento("name", "conContatoCelular", '33999999999');

        $this->procuraEPreencheUmElemento("name", "endNumero", $faker->numberBetween(1, 1000));
        
        //preenche email
        $this->procuraEPreencheUmElemento("name", "conContatoEmail", "teste@phpselenium.com");

        //aguarda preencher endereço pelo cep
        $this->aguardaElementoConterValor("name", "endLogradouro");

        ##Curso e Pagamento
        $this->clicaElemento("cssSelector", "a[href='#tab4']");


        //Seleciona período Letivo
        if(!PERIODO){
            $this->selecioneOptionQualquer("id", "s2id_per");
        }
        else{
            $this->clicaElemento("id","s2id_per");
            $this->aguardaElementoInvisivel("cssSelector","#select2-drop");
            $this->procuraEPreencheUmElemento('cssSelector', '#select2-drop > div > input', PERIODO);
            $this->aguardaElementoVisivel("cssSelector", ".select2-highlighted");
            $this->escolheOptionSelectContemValor("cssSelector","#select2-drop > ul > li", PERIODO);   
        }

        //Seleciona turma
        if(!TURMA){
            $this->selecioneOptionQualquer("id", "s2id_turma");
        }
        else{
            $this->clicaElemento("id","s2id_turma");
            $this->aguardaElementoInvisivel("cssSelector","#select2-drop");
            $this->procuraEPreencheUmElemento('cssSelector', '#select2-drop > div > input', TURMA);
            $this->aguardaElementoVisivel("cssSelector", ".select2-highlighted");
            $this->escolheOptionSelectContemValor("cssSelector","#select2-drop > ul > li", TURMA);   
        }
        //verifica se a unidade é obrigatória
        if($this->verificaElementoContemAtributo('cssSelector', 'span[for=UnidadeEstudo]', 'style', 'display: inline')){
            //faz cadastro unidade
            if(!UNIDADE_ESTUDO){
                $this->selecioneOptionQualquer("id", "s2id_unidadeEstudo");
            }
            else{
                $this->clicaElemento("id","s2id_unidadeEstudo");
                $this->aguardaElementoInvisivel("cssSelector","#select2-drop");
                $this->procuraEPreencheUmElemento('cssSelector', '#select2-drop > div > input', UNIDADE_ESTUDO);
                $this->aguardaElementoVisivel("cssSelector", ".select2-highlighted");
                $this->escolheOptionSelectContemValor("cssSelector","#select2-drop > ul > li", UNIDADE_ESTUDO);   
            }
        }
        
        //Financeiro
        $this->clicaElemento("cssSelector", "a[href='#tab5']");

        //aguarda o overlay depois de sair 
        $this->pegaTitulosMatricula("cssSelector", "#tipoTitulo > div.list.componente-list.ui-sortable");

        // finalizar a matrícula
        $this->clicaElemento("cssSelector", ".btn.btn-primary.submit");
        $this->aguardaElementoVisivel("cssSelector","#form-captacao-inicio");
        return $this;
    }
}
