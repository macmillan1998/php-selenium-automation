<?php

namespace TestesUniversa\Secretaria\Captacao\PageObject;

use TestesUniversa\Funcoes;
use Facebook\WebDriver\WebDriver;

class MatriculaAluno extends Funcoes{

    public WebDriver $driver;

    public function __construct(WebDriver $driver)
    {
        $this->driver = $driver;
    }

    public function matriculaAlunoCaptacaoInternaSimplificada($dadosAluno): self
    {

        $this->acessaLink('matricula/acadgeral-aluno-captacao/captacao');

        //aguarda aparecer o campo de cpf
        $this->aguardaElementoVisivel('name', 'pesCpf');
        
        //preenche cpf captação interna
        $this->preencheCpf('id', 'pesCpf', $dadosAluno['cpf']);

        $this->aguardaElementoVisivel('name', 'pesNome');

        //preenche nome
        $this->procuraEPreencheUmElemento("id", "pesNome", $dadosAluno['nome']);

        //preenche nascimento
        $this->procuraEPreencheUmElemento("id", "pesDataNascimento", $dadosAluno['data_nasc']);
        // Primeiro Passo Dados Pessoais 
        
        //unidade de estudo
        $this->clicaElemento("id", "s2id_unidade");
        $this->aguardaElementoInvisivel("cssSelector", ".select2-searching");
        $this->clicaElemento("cssSelector", "#select2-drop > ul > li", 0);

        //PREENCHESEXO
        $this->clicaElemento("id", "s2id_pesSexo");
        $this->aguardaElementoVisivel("cssSelector", "#select2-results-1 > li");
        $this->escolheOptionSelect("cssSelector", "#select2-results-1 > li", $dadosAluno['sexo']);
        

        ##Contato e endereço
        $this->clicaElemento("cssSelector", "a[href='#contatoEndereco']");

        //preenche cep
        $this->procuraEPreencheUmElemento("name", "endCep", $dadosAluno['cep']);

        //preenche numero
        $this->procuraEPreencheUmElemento("name", "endNumero", $dadosAluno['numero']);

        //preenche celular
        $this->procuraEPreencheUmElemento("name", "conContatoCelular", $dadosAluno['celular']);

        //preenche email
        $this->procuraEPreencheUmElemento("name", "conContatoEmail", "teste@phpselenium.com");

        //aguarda preencher endereço pelo cep
        $this->aguardaElementoConterValor("name", "endLogradouro");

        ##Curso e Pagamentos
        $this->clicaElemento("cssSelector", "a[href='#cursoFormacao']");

        //formacao
        $this->procuraEPreencheUmElemento("id", "formacaoInstituicao", '-');

        //nivel
        $this->clicaElemento("id","s2id_nivelEnsino");
        $this->aguardaElementoInvisivel("cssSelector",".select2-searching");
        $this->escolheOptionSelect("cssSelector","#select2-drop > ul > li", 'Pós-Graduação Lato Sensu');

        //curso
        if(!curso){
            $this->selecioneOptionQualquer("id", "s2id_campusCurso");
        }
        else{
            $this->clicaElemento("id","s2id_campusCurso");
            $this->aguardaElementoInvisivel("cssSelector",".select2-searching");
            $this->procuraEPreencheUmElemento('cssSelector', '#select2-drop > div > input', curso);
            $this->aguardaElementoVisivel("cssSelector", ".select2-highlighted");
            $this->escolheOptionSelectContemValor("cssSelector","#select2-drop > ul > li", curso);
            
        }

        //aguarda o overlay depois de sair 
        $this->aguardaElementoInvisivel("cssSelector","app-overlay");
        $this->aguardaElementoVisivel("cssSelector","#tipoTitulo > div.list.componente-list.ui-sortable");
        $this->pegaTitulosMatricula("cssSelector", "#tipoTitulo > div.list.componente-list.ui-sortable");

        // finalizar a matrícula
        $this->clicaElemento("cssSelector", ".btn.btn-primary.submit");
        $this->aguardaElementoVisivel("cssSelector","#form-captacao-inicio");
        return $this;
    }
}


#form-captacao-inicio
#div-cpf