<?php

use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\WebDriver;
use TestesUniversa\Login\PageObject\Login;
use TestesUniversa\Secretaria\Captacao\PageObject\MatriculaAluno;
use PHPUnit\Framework\TestCase;

include_once(__DIR__ . '/../../variaveisGlobais.php');

class MatriculaAlunoTest extends TestCase
{
    public static WebDriver $driver;

    public static function setUpBeforeClass(): void
    {
        $host = 'http://localhost:4444/wd/hub';
        self::$driver = RemoteWebDriver::create($host, DesiredCapabilities::chrome());
        self::$driver->manage()->window()->maximize();
    }

    public function testMatriculaAluno()
    {
        
        $paginaLogin = new Login(self::$driver);
        $paginaLogin->realizaLogin(Login, Senha);
        
        do{
            $dadosAluno = Login::retornaDadosPessoa();
            $paginaMatricula = new MatriculaAluno(self::$driver);
            $paginaMatricula->matriculaAlunoCaptacaoInternaSimplificada($dadosAluno);
        }while (False);

   }
}