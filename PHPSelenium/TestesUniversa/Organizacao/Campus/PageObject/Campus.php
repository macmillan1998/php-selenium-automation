<?php

namespace TestesUniversa\Organizacao\Campus\PageObject;

use TestesUniversa\Funcoes;
use Facebook\WebDriver\WebDriver;

class Campus extends Funcoes{

    public WebDriver $driver;

    public function __construct(WebDriver $driver)
    {
        $this->driver = $driver;
    }

    public function cadastraCampus(): self   
    {

        $this->acessaLink('organizacao/org-campus/add');

        $this->procuraEPreencheUmElemento('cssSelector', '#campNome', 'Nome Campus');

        //Instituição do Campus
        $this-> escolheOptionSelectAsterisco('id', 's2id_ies', instituicaoCampus);

        //Diretoria do Campus
        $this-> escolheOptionSelectAsterisco('id', 's2id_diretoriaPes', diretoriaCampus);

        //Secretaria do Campus
        $this-> escolheOptionSelectAsterisco('id', 's2id_secretariaPes', secretariaCampus);

        $this->procuraEPreencheUmElemento('cssSelector', '#campTelefone', '33999999898');
        $this->procuraEPreencheUmElemento('cssSelector', '#campEmail', 'sitecampus@phpselenium.com');
        $this->procuraEPreencheUmElemento('cssSelector', '#campSite', 'www.site.campus.selenium.com');

        //Passo 2 Prédios
        $this->clicaElemento("cssSelector", "a[href='#abaPredios']");

        //abre modal predio
        $this->clicaElemento("cssSelector", "#btn-add-predio");
        $this->aguardaElementoVisivel("cssSelector", "#modal-predio.modal.fade.in");

        //tipo prédio
        $this->clicaElemento("id", "s2id_predioTipo");
        $this->aguardaElementoInvisivel("cssSelector", ".select2-searching");
        $this->escolheOptionSelect("cssSelector", "#select2-drop > ul > li", tipoPredio);

        $this->procuraEPreencheUmElemento('cssSelector', '#predioNome', 'Nome Prédio Selenium');

        //endereço
        $this->procuraEPreencheUmElemento('cssSelector', '#predioCep', '35300118');
        $this->clicaElemento("cssSelector", "#predio-form .btn.btn-default.buscarCep");
        $this->procuraEPreencheUmElemento('cssSelector', '#predioNumero', 35);
        $this->aguardaElementoConterValor("id", "predioLogradouro");

        //salva predio
        $this->clicaElemento("cssSelector", "#btn-salvar-predio");
        //salva campus
        $this->clicaElemento("cssSelector", "#org-campus-form > div.col-sm-12.text-center > div > button");

        
        return $this;
    }


}