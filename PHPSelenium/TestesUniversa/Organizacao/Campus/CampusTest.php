<?php

namespace TestesUniversa\Organizacao\Campus;

use TestesUniversa\Organizacao\Campus\PageObject\Campus;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\WebDriver;
use TestesUniversa\Login\PageObject\Login;
use PHPUnit\Framework\TestCase;

include_once(__DIR__ . '/../../variaveisGlobais.php');

class CampusTest extends TestCase
{
    public static WebDriver $driver;

    public static function setUpBeforeClass(): void
    {
        $host = 'http://localhost:4444/wd/hub';
        self::$driver = RemoteWebDriver::create($host, DesiredCapabilities::chrome());
        self::$driver->manage()->window()->maximize();
    }

    public function testCadastraCampus()
    {
        $paginaLogin = new Login(self::$driver);
        $paginaLogin->realizaLogin(Login, Senha);

        $paginaCampus = new Campus(self::$driver);
        $paginaCampus->cadastraCampus();

    }
}