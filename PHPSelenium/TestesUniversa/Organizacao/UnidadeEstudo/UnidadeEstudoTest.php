<?php

namespace TestesUniversa\Organizacao\UnidadeEstudo;

use TestesUniversa\Organizacao\UnidadeEstudo\PageObject\UnidadeEstudo;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\WebDriver;
use TestesUniversa\Login\PageObject\Login;
use PHPUnit\Framework\TestCase;

include_once(__DIR__ . '/../../variaveisGlobais.php');

class UnidadeEstudoTest extends TestCase
{
    public static WebDriver $driver;

    public static function setUpBeforeClass(): void
    {
        $host = 'http://localhost:4444/wd/hub';
        self::$driver = RemoteWebDriver::create($host, DesiredCapabilities::chrome());
        self::$driver->manage()->window()->maximize();
    }

    public function testCadastraUnidadeEstudo()
    {
        $paginaLogin = new Login(self::$driver);
        $paginaLogin->realizaLogin(Login, Senha);

        $paginaUnidadeEstudo = new UnidadeEstudo(self::$driver);
        $paginaUnidadeEstudo->cadastraUnidadeEstudo();

    }
}