<?php

namespace TestesUniversa\Organizacao\UnidadeEstudo\PageObject;

use TestesUniversa\Funcoes;
use Facebook\WebDriver\WebDriver;
use Faker\Factory;

class UnidadeEstudo extends Funcoes{

    public WebDriver $driver;

    public function __construct(WebDriver $driver)
    {
        $this->driver = $driver;
    }

    public function cadastraUnidadeEstudo(): self   
    {
        $faker = Factory::create('pt_BR');
        
        $this->acessaLink('organizacao/org-unidade-estudo/add');
      
        $this->procuraEPreencheUmElemento('cssSelector', '#unidadeNome', $faker->name());

        //responsável pela UE
        $this-> escolheOptionSelectAsterisco('id', 's2id_pes', responsavelUnidade);

        //Campus da Unidade
        $this-> escolheOptionSelectAsterisco('id', 's2id_camp', campusUnidade);

        //Codigo da unidade no mec
        $this->procuraEPreencheUmElemento('cssSelector', '#unidadeCodMec', $faker->randomNumber(4));

        //Nome da unidade no e-mec
        $this->procuraEPreencheUmElemento('cssSelector', '#unidadeNomeMec', $faker->name());

        //Número de vagas da unidade
        $this->procuraEPreencheUmElemento('cssSelector', '#unidadeNumVagas', $faker->randomNumber(4));
        
        //Cep da unidade
        $this->procuraEPreencheUmElemento('cssSelector', '#unidadeEndCep', '35300107');
        //busca cep
        $this->clicaElemento('cssSelector', '#org-unidade-estudo-form > div:nth-child(10) > div > div > div');
        $this->aguardaElementoConterValor("id", "unidadeEndLogradouro");
        $this->procuraEPreencheUmElemento('cssSelector', '#unidadeEndNumero', $faker->numberBetween(1, 4000));

        //Telefone da unidade
        $this->procuraEPreencheUmElemento('cssSelector', '#unidadeTelefone', '33999999988');

        //Email da unidade
        $this->procuraEPreencheUmElemento('cssSelector', '#unidadeEmail', $faker->safeEmail());

        //Salva Unidade
        $this->clicaElemento('cssSelector', '#org-unidade-estudo-form > div.col-sm-12.text-center > div > button');

        return $this;
    }
}