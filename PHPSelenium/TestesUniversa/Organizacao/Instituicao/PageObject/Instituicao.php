<?php

namespace TestesUniversa\Organizacao\Instituicao\PageObject;

use TestesUniversa\Funcoes;
use Facebook\WebDriver\WebDriver;
use phpDocumentor\Reflection\PseudoTypes\True_;

class Instituicao extends Funcoes{

    public WebDriver $driver;

    public function __construct(WebDriver $driver)
    {
        $this->driver = $driver;
    }

    public function cadastraInstituicao(): self   
    {

        $this->acessaLink('organizacao/org-ies/add');

        //aguarda aparecer o campo de nome para começar o teste
        $this->aguardaElementoVisivel('cssSelector', '#iesNome');

        $this->procuraEPreencheUmElemento('cssSelector', '#iesNome', 'Instituição');

        $this->procuraEPreencheUmElemento('cssSelector', '#iesSigla', substr('Instituição', 0, 3));

        //seleciona a organização academica
        $this->clicaElemento("id", "s2id_iesOrganizacaoAcademica");
        $this->aguardaElementoInvisivel("cssSelector", ".select2-searching");
        $this->escolheOptionSelect("cssSelector", "#select2-drop > ul > li", orgAcademica);

        //IES REGISTRADORA?
        $this->clicaElemento("id", "s2id_iesRegistradora");
        $this->aguardaElementoInvisivel("cssSelector", ".select2-searching");
        $this->escolheOptionSelect("cssSelector", "#select2-drop > ul > li", registradora);
        
        if(possuiRegistradora){
            //SELECIONA IES REGISTRADORA
            $this->clicaElemento("id", "s2id_registradora");
            $this->aguardaElementoInvisivel("cssSelector", ".select2-searching");
            $this->escolheOptionSelect("cssSelector", "#select2-drop > ul > li", registradoraIes);
        }
        
        //Mantenedora
        $this-> escolheOptionSelectAsterisco('id', 's2id_pesMantenedora', mantenedora);

        //Diretora a cadêmica
        $this-> escolheOptionSelectAsterisco('id', 's2id_diretoriaPes', diretorAcademica);

        //Secretaria
        $this-> escolheOptionSelectAsterisco('id', 's2id_secretariaPes', secretaria);

        $this->procuraEPreencheUmElemento('cssSelector', '#iesNumeroMec', numeroMec);
        
        //endereço
        $this->procuraEPreencheUmElemento("id", "iesEndCep", '35300107');
        $this->clicaElemento("cssSelector", ".btn.btn-default.buscarCep");
        $this->procuraEPreencheUmElemento("id", "iesEndNumero", '89');
        $this->procuraEPreencheUmElemento("id", "iesTelefone", '33999989898');
        $this->procuraEPreencheUmElemento("id", "iesEmail", 'emailInstit@phpselenium.com');
        $this->aguardaElementoConterValor("id", "iesEndLogradouro");

        //passo 2 - caso necessário realizar as configurações para inserção de imagens
        //$this->clicaElemento("cssSelector", "a[href='#abaDefinicaoImagens']");

        $this->clicaElemento("cssSelector", "#org-ies-form > div.col-sm-12.text-center > div > button");

        
        return $this;
    }

    public function cadastraRegulacao(): self   
    {

        $this->acessaLink('organizacao/org-ies');

        //aguarda aparecer o campo de nome para começar o teste
        $this->aguardaElementoConterAtributo('cssSelector', '#dataTableOrgIes_processing', 'style', 'display: none');

        if(True){

            if($this->verificaSeExisteTextoListaDataTables('cssSelector', '#dataTableOrgIes > tbody > tr > td:first-child', 'Universidade de teste')){
                var_dump('ok');
            }
            else{
                var_dump('não');
            }

        }
        return $this;
    }


}