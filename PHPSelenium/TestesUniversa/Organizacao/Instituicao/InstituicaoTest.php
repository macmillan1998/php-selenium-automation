<?php

namespace TestesUniversa\Organizacao\Instituicao;

use TestesUniversa\Organizacao\Instituicao\PageObject\Instituicao;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\WebDriver;
use TestesUniversa\Login\PageObject\Login;
use PHPUnit\Framework\TestCase;

include_once(__DIR__ . '/../../variaveisGlobais.php');

class InstituicaoTest extends TestCase
{
    public static WebDriver $driver;

    public static function setUpBeforeClass(): void
    {
        $host = 'http://localhost:4444/wd/hub';
        self::$driver = RemoteWebDriver::create($host, DesiredCapabilities::chrome());
        self::$driver->manage()->window()->maximize();
    }

    // public function testCadastraInstituicao()
    // {
    //     $paginaLogin = new Login(self::$driver);
    //     $paginaLogin->realizaLogin(Login, Senha);

    //     $paginaInstituicao = new Instituicao(self::$driver);
    //     $paginaInstituicao->cadastraInstituicao();

    // }

    public function testCadastraRegulacao()
    {
        $paginaLogin = new Login(self::$driver);
        $paginaLogin->realizaLogin(Login, Senha);

        $paginaInstituicao = new Instituicao(self::$driver);
        $paginaInstituicao->cadastraRegulacao();

    }
}