<?php
//{cadastro de aluno
define('UrlBase', 'https://universa.sprint.sistemauniversa.com.br/'); //url base para testes
define('Login', 'marcos.macmillan'); //login do ambiente
define('Senha', 'marcos.macmillan'); //senha do login 
define('formaPagamento', 'Boleto'); //formas de pagamento para os títulos do aluno no ato da matrícula
define('tipoTitulos', ['Matricula', 'Mensalidade']); // tipo de título que será gerado para o aluno (escolha disponível apenas caso não seja obrigatório)
define('parcelas', ['3', '2']); // quantidade de parcelas dos títulos (caso tenha mais de um valor, ele selecionará o que encontrar pro tipo de título)
//}cadastro de aluno
define('curso', 'Curso de Pós Deferimento Automático e Deferido');
// define('curso', 'Curso de Pós Deferimento Manual e Deferido');

//{ *******************cadastro de curso**************************
define('cargaHorariaPraticaCurso', 100); //carga horária prática do curso valores:(int)
define('cargaHorariaTeoricaCurso', 200);  //carga horária teórica do curso valores:(int) 
define('modalidade', 'EAD'); //modalidade do curso (para cadastro do curso) valores:('EAD', 'Presencial', 'Semipresencial')
define('integralizacao', 'Mensal'); // tipo de cálculo da integralização (para cadastro do curso) valores:('Anual', 'Bimestral', 'Diario', 'Mensal', 'Quadrimestral', 'Semestral', 'Trimestral')
define('cursoPrazoIntegralizacao', 6); // data inicial de integralização (para cadastro do curso) valores:(int)
define('cursoPrazoIntegralizacaoMaxima', 12); //data final de integralização (para cadastro do curso) valores:(int)
define('nivelCurso', 'Pós-Graduação Lato Sensu'); //nível do curso (para cadastro do curso) valores:(níveis habilitados na instância)
define('temPeriodo', 'Não'); //possui período ou não (para cadastro do curso) valores:('Sim', 'Não')
define('areaConhecimento', 'Educação'); // área de conhecimento do curso valores:(tipo áreas de conhecimento cadastradas)
define('turnos', ['Integral', 'Noturno']); // turno disponíveis (para cadastro do curso) valores:('Integral','Noturno','Matutino','Vespertino')
define('vagasTurnos', 
['Integral' => 'cursoVagasIntegral',
'Noturno' => 'cursoVagasNoturno',
'Matutino' => 'cursoVagasMatutino',
'Vespertino' => 'cursoVagasVespertino']);//variável para controlar a variável de turnos, caso tenha mais de um selecionado (não necessário alterar)
define('titulosObrigatorios', ['Matrícula', 'Mensalidade']); //titulos obrigatórios (para cadastro do curso) valores:(tipos de titulos existentes)
define('titulosObrigatoriosExterno', []); //titulos opcionais internamente (para cadastro do curso) valores:(tipos de titulos existentes)
define('titulosOpcionais', []); //titulos opcionais (para cadastro do curso) valores:(tipos de titulos existentes)
define('deferimentoAuto', 'Sim'); //configuração de deferimento automático (para cadastro do curso) valores:('Sim', 'Não')
define('bloqueioInadimplencia', 'Não'); //configuração de bloqueio por inadimplência (para cadastro do curso) valores:('Sim', 'Não')
define('bloqueioDocumentos', 'Não'); //configuração de bloqueio por documento (para cadastro do curso) valores:('Sim', 'Não')
define('tempoReativacao', 100); //tempo de reativação do curso (para cadastro do curso) valores:(int)
define('posDeferimento', 'Deferido'); //situação pós deferimento valores:('Deferido', 'Aguardando Pagamento')
define('posMatricula', 'Deferido'); //situação pós matrícula valores:('Deferido', 'Pendente')
define('regimeDisciplinas', 'Bloco'); //regime de disciplinas valores:('Bloco', 'Crédito')
define('metodoAvaliacao', 'Nota'); // Método de avaliação valores:('Nota', 'Conceito')
define('minCredito', 4); // Método de avaliação valores:(int)
define('maxCredito', 15); // Método de avaliação valores:(int)
define('maxPendencia', 4); // máximo de pendência que um aluno pode ter
define('minFrequencia', 70); // mínimo de frequência para aprovação (%)
define('notaMax', 100); // nota máxima do período valores:(int)
define('encerramentoEtapa', 'Não'); //configuração de encerramento de etapas valores:('Sim', 'Não')
define('notaAprovacao', 70); // nota necessária para aprovação valores:(int)
define('calculoNotas', 'Soma'); // método de cálculo de notas valores:('Media', 'Soma')
define('qntdCasasDecimais', 2); // quantidade de casas decimais valores:(int)
define('minNotaFinal', 45); // valor mínimo para o aluno conseguir fazer exame final valores:(int)
define('minAprovacaoFinal', 70); // valor mínimo para o aluno ser aprovado no exame final valores:(int)
define('metodoCalculoFinal', 'Final'); // valor mínimo para o aluno ser aprovado no exame final valores:('Media', 'Final', 'Substitutiva')
define('qntdDisc', 1); // quantidade de disciplinas para o curso
define('criaDiscEMatriz', 3); // quantidade de disciplinas para o curso
//}************************

//{********** Cadastro de Disciplinas **************
define('areaConhecimentoDisc', 'Educação'); // área de conhecimento do curso valores:(tipo áreas de conhecimento cadastradas)
define('tipoDisc', 'Regular'); // Tipos de disciplina valores:(Regular, Optativa, Estágio, Equivalência, Base Diversificada, TCC)
define('CreditosPadrao', false); //caso seja criado disciplinas de crédito
define('nomeCurso', false); //caso seja criado disciplinas de crédito
//}

//{ ******* Cadastro de Matriz Curricular ************
define('cursoPadraoMatriz', false); //curso padrão caso queira criar apenas a matriz
define('vinculaMatriz', true); //curso padrão caso queira criar apenas a matriz

//}

//{ ******* Cadastro de Instituição ************
define('orgAcademica', 'Faculdade'); //Configuração padrão para o campo Organização Acadêmica valores: (Faculdade, Centro Universitário, Universidade, Educação Profissional, Escola)
define('registradora', 'Não'); //Se ela é registradora (Sim, Não)
define('possuiRegistradora', false); //Se ela possui registradora (Sim, Não)
define('registradoraIes', false); //Se ela possui registradora (Sim, Não)
define('mantenedora', false); //mantenedora específica (Sim, Não)
define('diretorAcademica', false); //mantenedora específica (Sim, Não)
define('secretaria', false); //mantenedora específica (Sim, Não)
define('credenciamento', false); //mantenedora específica (Sim, Não)
define('numeroMec', 255); //mantenedora específica (Sim, Não)
//}

//{ ******* Cadastro de Campus ************
define('instituicaoCampus', false); //Instituição para o campus, caso falso, seleciona qualquer uma
define('diretoriaCampus', false); //Diretoria para o campus, caso falso, seleciona qualquer uma
define('secretariaCampus', false); //Secretaria para o campus, caso falso, seleciona qualquer uma
define('tipoPredio', 'principal'); //prédio do campus valores:(apoio, principal)
//}

//{ ******* Cadastro de Unidade ************
define('responsavelUnidade', false); //Instituição para o Unidade, caso falso, seleciona qualquer uma
define('campusUnidade', false); //Instituição para o Unidade, caso falso, seleciona qualquer uma
//}
