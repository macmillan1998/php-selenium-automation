# Guia para uso

Para conseguir usar o PHP-Selenium, é necessário fazer alguns ajustes, após clonar o repositório.

1. Executar o composer.json (que está na pasta principal **PHPSelenium**)

2. Verificar a necessidade de instalar o JDK (sudo apt install default-jre)

3. Baixar o Selenium Server (já se encontra na raiz com o nome **selenium-server-4.1.1.jar**)

4. Baixar o ChromeDriver (o mesmo também já se encontra na raiz, porém, a versão que é executada no ChromeDriver **deve ser a mesma versão do Google Chrome** que está sendo usado na máquina local)

## Para Executar os testes

1. Execute o Servidor do Selenium usando o comando: **java -jar selenium-server-4.1.1.jar standalone**

2. Execute os testes. Um exemplo a ser seguido seria o de matrícula de um aluno: **php vendor/bin/phpunit TestesUniversa/matriculaAluno/MatriculaAlunoTest.php**
