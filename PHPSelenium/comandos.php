<?php
// Assert

use Facebook\WebDriver\WebDriverBy;
// tag
$drive->findElement(WebDriverBy::tagName('h1')); // procurar elemento por tag
$procuraTag = $driver->findElement(WebDriverBy::tagName('h1'))->getText(); 
self::assertSame('String1', $procuraTag);

// className
$driver->findElement(WebDriverBy::className('btn btn-dark'))->getText();
$procuraClassName = $driver->findElement(WebDriverBy::className('btn btn-dark'))->getText();
self::assertSame('String1', $procuraClassName);

// cssSelector
$driver->findElement(WebDriverBy::cssSelector('.btn.btn-dark'))->getText();
$procuraCssSelector = $driver->findElement(WebDriverBy::cssSelector('.btn.btn-dark'))->getText();
self::assertSame('String1', $procuraCssSelector);

// xpath
$driver->findElement(WebDriverBy::xpath('//a[@class="btn btn-dark mb-2"]'))->getText();
$procuraXpath = $driver->findElement(WebDriverBy::xpath('//a[@class="btn btn-dark mb-2"]'))->getText();
self::assertSame('String1', $procuraXpath);

// linkText
$driver->findElement(WebDriverBy::linkText('Entrar'))->getText();
$procuraxXath = $driver->findElement(WebDriverBy::linkText('Entrar'))->getText();
self::assertSame('String1', $procuralinkText);

// partialLinkText
$driver->findElement(WebDriverBy::partialLinkText('Entr'))->getText();
$procuraxXath = $driver->findElement(WebDriverBy::partialLinkText('Entr'))->getText();
self::assertSame('String1', $procurapartialLinkText);


// iframe
$iframe = $driver->findElement(WebDriverBy::tagName('iframe'));
$driver->switchTo()->frame($iframe);

// trocar de aba navegador
$drive->switchTo()->window($driver->getWindowHandles()[1]);

// acessar url
$driver->get("https://www.google.com");

// alterar tamanho navegador
$driver->manage()->window()->maximize();

// instanciar um navegador
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\Remote\RemoteWebElement;
use Facebook\WebDriver\WebDriverExpectedCondition;

require_once(__DIR__ . '/vendor/autoload.php');
$host = 'http://localhost:4444'; // this is the default
$driver = RemoteWebDriver::create($host, DesiredCapabilities::chrome());

// apertar enter
use Facebook\WebDriver\WebDriverKeys;

$driver->getKeyboard()->pressKey(WebDriverKeys::ENTER);
// 
$procuraxXath = $driver->findElement(WebDriverBy::partialLinkText('Entr'));
$procuraxXath->submit();
//
$procuraxXath = $driver->findElement(WebDriverBy::partialLinkText('Entr'))->submit();

// fazer verificações com assert
self::assertSame('https://www.google.com"', $driver->getCurrentURL()); // método pega url e verifica se é a da string
//
self::assertInstanceOf(RemoteWebElement::class, $driver->findElement(WebDriverBy::linkText('sair')));

// verificar se um elemento está ativo
self::assertInstanceOf(RemoteWebElement::class, $driver->findElement(WebDriverBy::linkText('sair'))->isDisplayed());

// classe para se tratar select
use Facebook\WebDriver\WebDriverSelect;
$elementoSelect = new WebDriverSelect($driver->findElement(WebDriverBy::id('nome'))); // pega o select 
$elementoSelect->selectByIndex(1); // pega elemento pelo posição
$elementoSelect->selectByValue('Pós Graduação'); // pega elemento pelo nome

// Enviar arquivos
$input->sendKeys('/tmp/imagem.jpg');

//caso a textarea não permita preencher algo, faça um click antes

// fechar navegador
$driver->close();

// limpar dados do input
self::assertInstanceOf(RemoteWebElement::class, $driver->findElement(WebDriverBy::linkText('sair'))->clear());




// waits em PHP+Selenium
$driver->wait()->until(WebDriverExpectedCondition::visibilityOf($elemento)); // Basicamente está dizendo, driver, espere, até aparecer tal elemento
// Wait for the page title to be 'My Page'. 

// Default wait (= 30 sec)
$driver->wait()->until(
    WebDriverExpectedCondition::titleIs('My Page')
  );
  
  
  // Wait for at most 10s and retry every 500ms if it the title is not correct.
  $driver->wait(10, 500)->until(
    WebDriverExpectedCondition::titleIs('My Page')
  );
//Wait for title
// titleIs()
// titleContains()
// titleMatches()

// Wait for URL
// urlIs()
// urlContains()
// urlMatches()

// Elements and their text
// presenceOfElementLocated()
// presenceOfAllElementsLocatedBy()
// elementTextIs()
// elementTextContains()
// elementTextMatches()
// textToBePresentInElementValue()

// Elements visibility and invisibility
// visibilityOfElementLocated()
// $driver->wait(10, 1000)->until(
//     WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::id('first_name'))
//   );
//   visibilityOf() - note the element must already be present in the DOM and you only wait until it became visible
//   $element = $driver->findElement(WebDriverBy::id('first_name'));
//   $driver->wait(10, 1000)->until(
//     WebDriverExpectedCondition::visibilityOf($element)
//   );
// invisibilityOfElementLocated()
// invisibilityOfElementWithText()
// Frames, windows, alerts
// frameToBeAvailableAndSwitchToIt()
// elementToBeClickable()
// alertIsPresent()
// numberOfWindowsToBe()
// Special conditions
// stalenessOf()
// refreshed()
// not()
// elementToBeSelected()
// elementSelectionStateToBe()
// Custom conditions
// The until() method accepts also any callable, providing you an possibility to implement custom condition like this:

// $driver->wait()->until(
//     function () use ($driver) {
//         $elements = $driver->findElements(WebDriverBy::cssSelector('li.foo'));

//         return count($elements) > 5;
//     },
//     'Error locating more than five elements'
// );
// This example condition will wait until more than 5 elements with selector li.foo appears.

// Implicit Waits
// An implicit wait is to tell WebDriver to poll the DOM for a certain amount of time when trying to find an element or elements if they are not immediately available. The default setting is 0. Once set, the implicit wait is set for the life of the WebDriver object instance.

// $driver->manage()->timeouts()->implicitlyWait(10);